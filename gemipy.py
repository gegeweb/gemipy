#!/usr/bin/env python3

import argparse
import configparser
import frontmatter
import glob
import os
import pathlib
import html2text
import sys
from datetime import datetime
from mastodon import Mastodon

### CONFIG
COMMENT_TITLE = "## Commentaires"

# Phrase indiquant qu'il n'y a pas de commentaires, le cas échéant
NO_COMMENT = "Encore aucun commentaire !"

# Local timezone (from server timezone)
# Toots date are in UTC, so need to be localized (or not)
# You can change it by using another timezone
# LOCAL_TIMEZONE = pytz.timezone('Europe/Paris')
LOCAL_TIMEZONE = datetime.utcnow().astimezone().tzinfo
# Date format in comment
DATE_FORMAT = "%d %b %Y %H:%M"

## Nothing to change here even if you know what you're doing
# path of this script
WORK_DIR = os.path.dirname(__file__)
# where read the source files
SOURCE_DIR = os.path.join(WORK_DIR, "source")
# where write the live files
DEST_DIR = os.path.join(WORK_DIR, "dest")
# where is template file
TEMPLATE_DIR = os.path.join(WORK_DIR, "tpl/")
# teplate file
COMMENT_TPL = os.path.join(TEMPLATE_DIR, "comments.tpl")
# This string in the source file is replaced by the comments
COMMENT_PLACE = "%%commentaires%%"
## END OF CONFIG SECTION

# Functions called by CLI
def base_config():
    capsule_config()
    fedi_config()


def capsule_config():
    """Initiate capsule configuration and write config to ini file"""
    capsule_url = input("Enter URL of your Gemini capsule: ")
    if capsule_url:
        if capsule_url[:9] != "gemini://":
            capsule_url = f"gemini://{capsule_url}"
        if capsule_url[-1] != "/":
            capsule_url = f"{capsule_url}/"
        config["CAPSULE"] = {"URL": capsule_url}
        config.write_configfile()


def fedi_config():
    """Initiate interactive configuration of Fediverse connection
    and write config to ini file
    """
    fedi_instance = input("Enter URL of your Fediverse instance: ")
    fedi_username = input("Enter your username on this Fediverse instance: ")
    fedi_password = input("Finally, enter your Fediverse account password: ")

    if all([fedi_instance, fedi_username, fedi_password]):
        # Registration of the application
        try:
            Mastodon.create_app(
                "gemipy",
                api_base_url=fedi_instance,
                to_file="gemipy_clientcred.secret",
            )
            app_created = True
        except:
            print("An error occured with the creation of app")

        if app_created:
            try:
                mastodon = Mastodon(
                    client_id="gemipy_clientcred.secret",
                    api_base_url=fedi_instance,
                )
                mastodon.log_in(
                    fedi_username,
                    fedi_password,
                    to_file="gemipy_usercred.secret",
                )
                print("Configuraton successful!")
                config["FEDIVERSE"] = {
                    "url": fedi_instance,
                    "usercred": "gemipy_usercred.secret",
                }
                config.write_configfile()
            except:
                print(
                    "An error occured with login. Wrong username or password?"
                )


# / Functions called by CLI


class App:
    """Store settings of the session"""

    def __init__(self, capsule_url, fedi_url, usercred):
        self.capsule_url = capsule_url
        self.fedi_url = fedi_url
        self.usercred = usercred
        self.fedi_connection = ""

        self.fedi_connect()

    def fedi_connect(self):
        """Connect to Mastodon/Pleroma"""
        self.fedi_connection = Mastodon(
            access_token=self.usercred, api_base_url=self.fedi_url
        )
        return True


class Config(configparser.ConfigParser):
    def __init__(self):
        super().__init__()
        self.file = os.path.join(WORK_DIR, "config.ini")

    def is_configfile_created(self):
        """Test if ini config file exists"""
        if os.path.isfile(self.file):
            return True
        return False

    def test_minimal_config(self):
        self.parse_configfile()

        try:
            self.get("CAPSULE", "URL")
        except configparser.NoSectionError:
            print("Config file: section CAPSULE missing")
            exit()
        except configparser.NoOptionError:
            print("Config file: Section CAPSULE: missing URL option")
            exit()

        try:
            self.get("FEDIVERSE", "Usercred")
        except configparser.NoSectionError:
            print("Config file: section FEDIVERSE missing")
            exit()
        except configparser.NoOptionError:
            print("Config file: Section FEDIVERSE: missing Usercred option")
            exit()

        try:
            self.get("FEDIVERSE", "URL")
        except configparser.NoSectionError:
            print("Config file: section FEDIVERSE missing")
            exit()
        except configparser.NoOptionError:
            print("Config file: Section FEDIVERSE: missing URL option")
            exit()

        return True

    def parse_configfile(self):
        self.read(self.file)

    def write_configfile(self):
        with open(self.file, "w") as configfile:
            self.write(configfile)


class Article:
    """Un article est une gempage"""

    def __init__(self, file):
        self.sourcefile = file
        # Chemin de destination de l'article
        self.destfile = ""
        # URL d'accès à l'article sur Gemini
        self.url = ""
        # flag to refresh or not the front of source file
        self.flag_refresh = False
        # flag to rewrite or not the dest file
        self.flag_rewrite = False
        # thread (/published toot) informations
        self.thread = {}

        with open(self.sourcefile) as f:
            self.article = frontmatter.loads(f.read())

        self.body_content = self.article.content
        self.toot_id = (
            self.article["toot-id"] if "toot-id" in self.article else ""
        )
        self.comments_nb = (
            self.article["comments-nb"] if "comments-nb" in self.article else ""
        )
        self.retoots_nb = (
            self.article["retoots-nb"] if "retoots-nb" in self.article else ""
        )
        self.display_comments = (
            self.article["comments"] if "comments" in self.article else True
        )

        # if toot-id is set, get info on published toot
        if self.toot_id:
            (
                self.thread["url"],
                self.thread["replies"],
                self.thread["retoots"],
            ) = self.get_toot_info()

        # if there are new replies since the last execution of script, set
        # flag_refresh to tue
        if "replies" in self.thread:
            if not self.comments_nb or (
                self.comments_nb
                and int(self.thread["replies"]) > int(self.comments_nb)
            ):
                self.flag_refresh = self.flag_rewrite = True
            self.comments_nb = self.thread["replies"]

        # if there are new retoots since the last execution of script, set
        # flag_refresh to true
        if "retoots" in self.thread:
            if not self.retoots_nb or (
                self.retoots_nb
                and int(self.thread["retoots"]) > int(self.retoots_nb)
            ):
                self.flag_refresh = self.flag_rewrite = True
            self.retoots_nb = self.thread["retoots"]

        # set path to dest file and url to gempage
        gmi_temp_file = os.path.relpath(self.sourcefile, SOURCE_DIR)
        self.destfile = os.path.join(DEST_DIR, gmi_temp_file)
        self.url = app.capsule_url + gmi_temp_file

        # if source file is more recent than dest file, set flag_refresh to true
        if os.path.isfile(self.destfile):
            dest_ctime = pathlib.Path(self.destfile).stat().st_ctime
            source_ctime = pathlib.Path(self.sourcefile).stat().st_ctime
            if source_ctime > dest_ctime:
                self.flag_rewrite = True
        else:
            self.flag_rewrite = True

        if self.flag_refresh:
            self.refresh_source_article()

    def get_toot_info(self):
        """Get information about published toot"""
        toot = app.fedi_connection.status(self.toot_id)
        return (toot["url"], toot["replies_count"], toot["reblogs_count"])

    def publish_toot(self):
        """Poste un toot pour publiciser l'article"""
        post = self.body_content
        length = 400 if len(post) > 400 else len(post)
        skip = " […]" if length >= 400 else ""
        post = self.body_content[:length]
        post = f"{post}{skip}\n\n{self.url}"
        toot = app.fedi_connection.status_post(
            post,
            # for test only
            # visibility = "direct"
        )

        if toot:
            self.toot_id = toot["id"]
            self.get_toot_info()
            return True
        return False

    def refresh_source_article(self):
        """Refresh frontmatter of source article"""
        self.article["comments-nb"] = self.comments_nb
        self.article["retoots-nb"] = self.retoots_nb
        self.article["toot-id"] = self.toot_id

        with open(self.sourcefile, "w") as f:
            f.write(frontmatter.dumps(self.article))

    def write_dest_article(self, force=False):
        if any([force, self.flag_rewrite]):
            final_content = self.body_content
            # création du sous-dossier s'il n'existe pas
            if not os.path.exists(os.path.dirname(self.destfile)):
                os.makedirs(os.path.dirname(self.destfile))

            if self.display_comments:
                comments = Comments(self.toot_id).get_toots()
                if comments:
                    final_content = final_content.replace(
                        COMMENT_PLACE, comments
                    )

            # replace keywords
            final_content = final_content.replace(
                "%%thread-url", self.thread["url"]
            )
            final_content = final_content.replace(
                "%%thread-replies", str(self.comments_nb)
            )
            final_content = final_content.replace(
                "%%thread-retoots", str(self.retoots_nb)
            )

            # écriture de l'article "final"
            with open(self.destfile, "w") as f:
                f.write(final_content)

            # On toote si les conditions sont réunies
            if "toot" in self.article.keys() and self.article["toot"] is True:
                # Si un toot_id est défini, on ne toote pas
                if not self.toot_id:
                    self.publish_toot()

            return True


class Comments:
    """Ensemble des toots composant les commentaires d'un article"""

    def __init__(
        self, toot_id, comment_title=COMMENT_TITLE, no_comment=NO_COMMENT
    ):
        self.toot_id = toot_id
        self.url = app.fedi_url
        self.comment_title = comment_title
        self.no_comment = no_comment
        self.comments = app.fedi_connection.status_context(toot_id)[
            "descendants"
        ]

    def get_toots(self):
        """Renvoie les toots d'un thread, formatés pour l'intégration dans la
        gempage
        """

        tpl = Template(COMMENT_TPL, "%%comments-begin", "%%comments-end")

        self.output = tpl.header

        if self.comments:
            for toot in self.comments:
                if LOCAL_TIMEZONE:
                    toot_date = (
                        toot["created_at"]
                        .astimezone(LOCAL_TIMEZONE)
                        .strftime(DATE_FORMAT)
                    )
                else:
                    toot_date = toot["created_at"].strftime(DATE_FORMAT)

                toot_content = html2text.HTML2Text()
                toot_content.ignore_links = True
                toot_content = toot_content.handle(toot["content"])
                comment = tpl.repeatable_part
                replacement = (
                    ("%%toot-author", toot["account"]["display_name"]),
                    ("%%toot-date", toot_date),
                    ("%%toot-content", toot_content),
                    ("%%toot-url", toot["url"]),
                )
                for r in replacement:
                    comment = comment.replace(r[0], r[1])

                self.output += comment

            self.output += tpl.footer

        else:
            self.output += self.no_comment
        return self.output


class Template:
    """Fournit des méthodes pour isoler les parties fixes d'un template, de la
    partie répétable. Un template est ainsi composé (i) d'un header, partie fixe
    positionnée avant la partie répétable, (ii) d'une partie répétable, (iii)
    d'un footer, partie fixe positionnée après la partie répétable
    La partie répétable est celle située entre tag_repeat_begin et
    tag_repeat_end
    """

    def __init__(self, template, tag_repeat_begin, tag_repeat_end):
        self.template = template
        self.content = ""
        self.tag_repeat_begin = tag_repeat_begin + "\n"
        self.tag_repeat_end = tag_repeat_end + "\n"
        self.header = ""
        self.repeatable_part = ""
        self.footer = ""
        self.repeatable_part_exists = False

        self.read_file()

    def read_file(self):
        with open(self.template, "r") as file:
            self.content = file.readlines()

        if (
            self.tag_repeat_begin in self.content
            and self.tag_repeat_end in self.content
        ):
            self.repeatable_part_exists = True
            self.trb_index = self.content.index(self.tag_repeat_begin)
            self.tre_index = self.content.index(self.tag_repeat_end)
            self.extract_header()
            self.extract_footer()
            self.extract_repeatable_part()

    def extract_header(self):
        self.header = "".join(self.content[: self.trb_index])
        return True

    def extract_footer(self):
        self.footer = "".join(self.content[self.tre_index + 1 :])
        return True

    def extract_repeatable_part(self):
        self.repeatable_part = "".join(
            self.content[self.trb_index + 1 : self.tre_index]
        )
        return True


if __name__ == "__main__":
    config = Config()

    parser = argparse.ArgumentParser(
        description="Generate gempages including toots as comments"
    )
    parser.add_argument(
        "--config-capsule",
        action="store_true",
        help="Initiate interactive configuration for setting URL of your Gemini capsule",
    )
    parser.add_argument(
        "--config-fediverse",
        action="store_true",
        help="Initiate interactive configuration of Fediverse connection",
    )
    parser.add_argument(
        "--config-base",
        action="store_true",
        help="Initiate interactive basic configuration (Gemini + Fediverse)",
    )
    parser.add_argument(
        "--capsule-url", type=str, help="URL of your Gemini capsule"
    )
    parser.add_argument(
        "--fediverse-url",
        type=str,
        help="URL of your Mastodon or Pleroma instance",
    )
    parser.add_argument(
        "--usercred", type=str, help="Location of Fediverse user credentials"
    )
    args = parser.parse_args()

    # First, handle of "--config" options
    if config.is_configfile_created:
        config.parse_configfile()

    if any([args.config_fediverse, args.config_capsule, args.config_base]):
        if args.config_fediverse:
            fedi_config()
            exit()
        elif args.config_capsule:
            capsule_config()
            exit()
        elif args.config_base:
            base_config()
            exit()
    else:
        # No "--config" options : load settings from config file if exists then
        # override settings defined in CLI
        if not config.is_configfile_created() and not all(
            [args.capsule_url, args.fediverse_url, args.usercred]
        ):
            print(
                "Config file doesn't seem to exist and you didn't pass all necessary options. Please launch app with --config-base option to create it or specify settings using command line options"
            )
            exit()
        else:
            if config.is_configfile_created():
                config.test_minimal_config()
                capsule_url = config["CAPSULE"]["url"]
                fedi_url = config["FEDIVERSE"]["url"]
                usercred = config["FEDIVERSE"]["usercred"]

            if args.capsule_url:
                capsule_url = args.capsule_url
            if args.fediverse_url:
                fedi_url = args.fediverse_url
            if args.usercred:
                usercred = args.usercred

            app = App(capsule_url, fedi_url, usercred)

            # On parcourt de manière récursive le dossier "source" à la recherche des
            # fichiers *.gmi
            for gmi_file in glob.glob(SOURCE_DIR + "/**/*.gmi", recursive=True):
                article = Article(gmi_file)
                article.write_dest_article()
